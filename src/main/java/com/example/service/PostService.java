package com.example.service;

import com.example.entity.Post;
import com.baomidou.mybatisplus.extension.service.IService;

public interface PostService extends IService<Post> {


    void initIndexWeekRank();
}


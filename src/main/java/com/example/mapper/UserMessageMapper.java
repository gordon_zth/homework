package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.UserMessage;

public interface UserMessageMapper extends BaseMapper<UserMessage> {
}
package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.UserAction;

public interface UserActionMapper extends BaseMapper<UserAction> {
}
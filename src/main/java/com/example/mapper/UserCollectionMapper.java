package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.UserCollection;

public interface UserCollectionMapper extends BaseMapper<UserCollection> {
}
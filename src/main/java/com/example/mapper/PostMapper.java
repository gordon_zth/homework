package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.Post;

public interface PostMapper extends BaseMapper<Post> {
}
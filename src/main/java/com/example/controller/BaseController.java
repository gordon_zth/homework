package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName BaseController
 * @Author zhangtianheng
 * @Description
 * @Date 2020/1/2 16:12
 **/
public class BaseController {

    @Autowired
    HttpServletRequest req;
}

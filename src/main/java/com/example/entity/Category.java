package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "category")
public class Category extends BaseEntity {
    /**
     * 标题
     */
    @TableField(value = "name")
    private String name;

    /**
     * 内容描述
     */
    @TableField(value = "content")
    private String content;

    @TableField(value = "summary")
    private String summary;

    /**
     * 图标
     */
    @TableField(value = "icon")
    private String icon;

    /**
     * 该分类的内容数量
     */
    @TableField(value = "post_count")
    private Integer postCount;

    /**
     * 排序编码
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 父级分类的ID
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * SEO关键字
     */
    @TableField(value = "meta_keywords")
    private String metaKeywords;

    /**
     * SEO描述内容
     */
    @TableField(value = "meta_description")
    private String metaDescription;
}
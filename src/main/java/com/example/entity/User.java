package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "user")
public class User extends BaseEntity {
    /**
     * 昵称
     */
    @TableField(value = "username")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 邮件
     */
    @TableField(value = "email")
    private String email;

    /**
     * 手机电话
     */
    @TableField(value = "mobile")
    private String mobile;

    /**
     * 积分
     */
    @TableField(value = "point")
    private Integer point;

    /**
     * 个性签名
     */
    @TableField(value = "sign")
    private String sign;

    /**
     * 性别
     */
    @TableField(value = "gender")
    private String gender;

    /**
     * 微信号
     */
    @TableField(value = "wechat")
    private String wechat;

    /**
     * vip等级
     */
    @TableField(value = "vip_level")
    private Integer vipLevel;

    /**
     * 生日
     */
    @TableField(value = "birthday")
    private Date birthday;

    /**
     * 头像
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 内容数量
     */
    @TableField(value = "post_count")
    private Integer postCount;

    /**
     * 评论数量
     */
    @TableField(value = "comment_count")
    private Integer commentCount;

    /**
     * 状态
     */
    @TableField(value = "status")
    private Byte status;

    /**
     * 最后的登陆时间
     */
    @TableField(value = "lasted")
    private Date lasted;
}
package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "user_action")
public class UserAction extends BaseEntity {
    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 动作类型
     */
    @TableField(value = "action")
    private String action;

    /**
     * 得分
     */
    @TableField(value = "point")
    private Integer point;

    /**
     * 关联的帖子ID
     */
    @TableField(value = "post_id")
    private String postId;

    /**
     * 关联的评论ID
     */
    @TableField(value = "comment_id")
    private String commentId;
}
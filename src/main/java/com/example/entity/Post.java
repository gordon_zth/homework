package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "post")
public class Post extends BaseEntity {
    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 编辑模式：html可视化，markdown ..
     */
    @TableField(value = "edit_mode")
    private String editMode;

    @TableField(value = "category_id")
    private Long categoryId;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 支持人数
     */
    @TableField(value = "vote_up")
    private Integer voteUp;

    /**
     * 反对人数
     */
    @TableField(value = "vote_down")
    private Integer voteDown;

    /**
     * 访问量
     */
    @TableField(value = "view_count")
    private Integer viewCount;

    /**
     * 评论数量
     */
    @TableField(value = "comment_count")
    private Integer commentCount;

    /**
     * 是否为精华
     */
    @TableField(value = "recommend")
    private Boolean recommend;

    /**
     * 置顶等级
     */
    @TableField(value = "level")
    private Byte level;

    /**
     * 状态
     */
    @TableField(value = "status")
    private Byte status;
}
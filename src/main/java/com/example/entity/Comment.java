package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "comment")
public class Comment extends BaseEntity {
    /**
     * 评论的内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 回复的评论ID
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 评论的内容ID
     */
    @TableField(value = "post_id")
    private Long postId;

    /**
     * 评论的用户ID
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * “顶”的数量
     */
    @TableField(value = "vote_up")
    private Integer voteUp;

    /**
     * “踩”的数量
     */
    @TableField(value = "vote_down")
    private Integer voteDown;

    /**
     * 置顶等级
     */
    @TableField(value = "level")
    private Byte level;

    /**
     * 评论的状态
     */
    @TableField(value = "status")
    private Byte status;
}
package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "user_message")
public class UserMessage extends BaseEntity {
    /**
     * 发送消息的用户ID
     */
    @TableField(value = "from_user_id")
    private Long fromUserId;

    /**
     * 接收消息的用户ID
     */
    @TableField(value = "to_user_id")
    private Long toUserId;

    /**
     * 消息可能关联的帖子
     */
    @TableField(value = "post_id")
    private Long postId;

    /**
     * 消息可能关联的评论
     */
    @TableField(value = "comment_id")
    private Long commentId;

    @TableField(value = "content")
    private String content;

    /**
     * 消息类型
     */
    @TableField(value = "type")
    private Byte type;

    @TableField(value = "status")
    private Integer status;
}
package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "user_collection")
public class UserCollection extends BaseEntity {
    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "post_id")
    private Long postId;

    @TableField(value = "post_user_id")
    private Long postUserId;
}